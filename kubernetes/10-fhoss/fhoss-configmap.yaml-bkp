apiVersion: v1
data:
  DiameterPeerHSS.xml: "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- HSS Server
    config -->\n<DiameterPeer \n        FQDN=\"hss.IMS_DOMAIN\"\n        Realm=\"IMS_DOMAIN\"\n
    \       Vendor_Id=\"10415\"\n        Product_Name=\"JavaDiameterPeer\"\n        AcceptUnknownPeers=\"1\"\n
    \       DropUnknownOnDisconnect=\"1\"\n        Tc=\"30\"\n        Workers=\"4\"\n
    \       QueueLength=\"32\"\n>\n        <Peer FQDN=\"icscf.IMS_DOMAIN\" Realm=\"IMS_DOMAIN\"
    port=\"3869\" /> \n        <Peer FQDN=\"scscf.IMS_DOMAIN\" Realm=\"IMS_DOMAIN\"
    port=\"3870\" /> \n\n        <Acceptor port=\"3868\" bind=\"FHOSS_IP\" />\n\n
    \       <Auth id=\"16777216\" vendor=\"10415\"/><!-- 3GPP Cx -->\n        <Auth
    id=\"16777216\" vendor=\"4491\"/><!-- CableLabs Cx -->\n        <Auth id=\"16777216\"
    vendor=\"13019\"/><!-- ETSI/TISPAN Cx -->\n        <Auth id=\"16777216\" vendor=\"0\"/><!--
    ETSI/TISPAN Cx -->\n        <Auth id=\"16777217\" vendor=\"10415\"/><!-- 3GPP
    Sh -->\n        <Auth id=\"16777221\" vendor=\"10415\"/>\n\n</DiameterPeer>"
  configurator.sh: "#!/bin/bash\n\n# Initialization & global vars\n# if you execute
    this script for the second time\n# you should change these variables to the latest\n#
    domain name and ip address\nDDOMAIN=\"open-ims\\.test\"\nDSDOMAIN=\"open-ims\\\\\\.test\"\nDEFAULTIP=\"127\\.0\\.0\\.1\"\nCONFFILES=`ls
    *.cfg *.xml *.sql *.properties 2>/dev/null`\n\n# Interaction\ndomainname=$1\nip_address=$2\n\n#
    input domain is to be slashed for cfg regexes \nslasheddomain=`echo $domainname
    | sed 's/\\./\\\\\\\\\\\\\\\\\\./g'`\n\nprintf \"changing: \"\nfor i in $CONFFILES
    \ndo\nsed -i -e \"s/$DDOMAIN/$domainname/g\" $i\nsed -i -e \"s/$DSDOMAIN/$slasheddomain/g\"
    $i\nsed -i -e \"s/$DEFAULTIP/$ip_address/g\" $i\n\nprintf \"$i \" \ndone \necho"
  fhoss_init.sh: |-
    #!/bin/bash
    export IP_ADDR=$(awk 'END{print $1}' /etc/hosts)

    [ ${#MNC} == 3 ] && IMS_DOMAIN="ims.mnc${MNC}.mcc${MCC}.3gppnetwork.org" || IMS_DOMAIN="ims.mnc0${MNC}.mcc${MCC}.3gppnetwork.org"

    cp /mnt/fhoss/configurator.sh /opt/OpenIMSCore/FHoSS/deploy
    cp /mnt/fhoss/DiameterPeerHSS.xml /opt/OpenIMSCore/FHoSS/deploy
    cp /mnt/fhoss/hibernate.properties /opt/OpenIMSCore/FHoSS/deploy
    cp /mnt/fhoss/configurator.sh /opt/OpenIMSCore/FHoSS/scripts
    cp /mnt/fhoss/configurator.sh /opt/OpenIMSCore/FHoSS/config

    cd /opt/OpenIMSCore/FHoSS/deploy && ./configurator.sh ${IMS_DOMAIN} ${FHOSS_IP}
    sed -i 's|open-ims.org|'$IMS_DOMAIN'|g' /opt/OpenIMSCore/FHoSS/deploy/webapps/hss.web.console/WEB-INF/web.xml
    sed -i 's|MYSQL_IP|'$MYSQL_IP'|g' /opt/OpenIMSCore/FHoSS/deploy/hibernate.properties
    sed -i 's|FHOSS_IP|'$IP_ADDR'|g' /opt/OpenIMSCore/FHoSS/deploy/DiameterPeerHSS.xml
    sed -i 's|IMS_DOMAIN|'$IMS_DOMAIN'|g' /opt/OpenIMSCore/FHoSS/deploy/DiameterPeerHSS.xml
    cd /opt/OpenIMSCore/FHoSS/scripts && ./configurator.sh ${IMS_DOMAIN} ${FHOSS_IP}
    cd /opt/OpenIMSCore/FHoSS/config && ./configurator.sh ${IMS_DOMAIN} ${FHOSS_IP}
    sed -i 's|open-ims.org|'$IMS_DOMAIN'|g' /opt/OpenIMSCore/FHoSS/src-web/WEB-INF/web.xml

    while ! mysqladmin ping -h ${MYSQL_IP} --silent; do
            sleep 5;
    done

    # Sleep until permissions are set
    sleep 10;

    # Create FHoSS database, populate tables and grant privileges
    if [[ -z "`mysql -u root -h ${MYSQL_IP} -qfsBe "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='hss_db'" 2>&1`" ]];
    then
            mysql -u root -h ${MYSQL_IP} -e "create database hss_db;"
            mysql -u root -h ${MYSQL_IP} hss_db < /opt/OpenIMSCore/FHoSS/scripts/hss_db.sql
            FHOSS_USER_EXISTS=`mysql -u root -h ${MYSQL_IP} -s -N -e "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE User = 'hss' AND Host = '%')"`
            if [[ "$FHOSS_USER_EXISTS" == 0 ]]
            then
                    mysql -u root -h ${MYSQL_IP} -e "CREATE USER 'hss'@'%' IDENTIFIED WITH mysql_native_password BY 'hss'";
                    mysql -u root -h ${MYSQL_IP} -e "CREATE USER 'hss'@'$FHOSS_IP' IDENTIFIED WITH mysql_native_password BY 'hss'";
                    mysql -u root -h ${MYSQL_IP} -e "GRANT ALL ON hss_db.* TO 'hss'@'%'";
                    mysql -u root -h ${MYSQL_IP} -e "GRANT ALL ON hss_db.* TO 'hss'@'$FHOSS_IP'";
                    mysql -u root -h ${MYSQL_IP} -e "FLUSH PRIVILEGES;"
            fi
            mysql -u root -h ${MYSQL_IP} hss_db < /opt/OpenIMSCore/FHoSS/scripts/userdata.sql
    fi

    VIS_NET_PRESENT=`mysql -u root -h ${MYSQL_IP} hss_db -s -N -e "SELECT count(*) FROM visited_network;"`
    if [[ "$VIS_NET_PRESENT" -gt 0 ]]
    then
            mysql -u root -h ${MYSQL_IP} hss_db -e "DELETE FROM visited_network;"
            mysql -u root -h ${MYSQL_IP} hss_db -e "INSERT INTO visited_network VALUES (1, '$IMS_DOMAIN');"
    fi

    PREF_SCSCF_PRESENT=`mysql -u root -h ${MYSQL_IP} hss_db -s -N -e "SELECT count(*) FROM preferred_scscf_set;"`
    if [[ "$PREF_SCSCF_PRESENT" -gt 0 ]]
    then
            mysql -u root -h ${MYSQL_IP} hss_db -e "DELETE FROM preferred_scscf_set;"
            mysql -u root -h ${MYSQL_IP} hss_db -e "INSERT INTO preferred_scscf_set VALUES (1, 1, 'scscf1', 'sip:scscf.$IMS_DOMAIN:6060', 0);"
    fi

    # Sync docker time
    #ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

    cp /mnt/fhoss/hss.sh /
    cd / && ./hss.sh
  hibernate.properties: |-
    ## MySQL

    # hibernate configuration
    hibernate.dialect=org.hibernate.dialect.MySQLDialect
    #hibernate.connection.driver_class=org.gjt.mm.mysql.Driver
    hibernate.connection.driver_class=com.mysql.jdbc.Driver
    hibernate.connection.url=jdbc:mysql://MYSQL_IP:3306/hss_db
    hibernate.connection.username=hss
    hibernate.connection.password=hss
    hibernate.connection.isolation=1
    hibernate.connection.characterEncoding=utf8
    hibernate.connection.useUnicode=true
    hibernate.connection.CharSet=utf8

    # C3P0 configuration
    hibernate.c3p0.acquire_increment=1
    hibernate.c3p0.min_size=1
    hibernate.c3p0.max_size=30
    hibernate.c3p0.timeout=3600
    hibernate.c3p0.max_statements=0
    hibernate.c3p0.idle_test_period=1200
  hss.sh: |-
    #!/bin/bash
    # --------------------------------------------------------------
    # Include JAR Files
    # --------------------------------------------------------------

    cd /opt/OpenIMSCore/FHoSS/deploy
    JAVA_HOME="/usr/lib/jvm/jdk1.7.0_80"
    CLASSPATH="/usr/lib/jvm/jdk1.7.0_80/jre/lib/"
    echo "Building Classpath"
    CLASSPATH=$CLASSPATH:log4j.properties:.
    for i in lib/*.jar; do CLASSPATH="$i":"$CLASSPATH"; done
    echo "Classpath is $CLASSPATH."

    # --------------------------------------------------------------
    # Start-up
    # --------------------------------------------------------------

    $JAVA_HOME/bin/java -cp $CLASSPATH de.fhg.fokus.hss.main.HSSContainer $1 $2 $3 $4 $5 $6 $7 $8 $9
kind: ConfigMap
metadata:
  creationTimestamp: "2022-02-11T01:16:03Z"
  name: fhoss
  namespace: open5gs-migrate
  resourceVersion: "9093027"
  uid: ebf04e23-6d96-42ac-9c9d-72aa8c21421a
